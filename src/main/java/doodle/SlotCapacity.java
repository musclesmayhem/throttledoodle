package doodle;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * Created by Karin on 1/28/2016.
 */

@Data
@Log4j2
public class SlotCapacity {

    private int[] capacity;

    public SlotCapacity(Integer slots) {
        capacity = new int[slots];
    }

    public Integer get(Integer index) {
        return capacity[index];
    }

    public int[] assign(Integer load) {
        for (int i = 0; i < capacity.length; i++) {
            capacity[i] = load;
        }
        log.debug(this);
        return capacity;
    }

    public int[] assign(Integer index, Integer load) {
        capacity[index] = load;
        log.debug(this);
        return capacity;
    }

    public int[] increment(Integer index) {
        capacity[index]++;
        log.debug(this);
        return capacity;
    }

    public int[] assignWithAllowance(Integer load, Float allowance) {

        // Calculate allowance first
        int allowanceLoad = Math.round(load * allowance);
        log.debug("allowance load: {}", allowanceLoad);

        // Calculate remaining slot values
        int slotLoad = (load - allowanceLoad) / (capacity.length - 1);
        int remainderLoad = (load - allowanceLoad) % (capacity.length - 1);

        log.debug("slot load: {}", slotLoad);
        log.debug("remainder load: {}", remainderLoad);

        // Assign allowance to the last slot
        capacity[capacity.length - 1] = allowanceLoad;

        // Distribute load evenly across the remaining threads
        for (int i = 0; i < capacity.length - 1; i++) {
            assign(i, slotLoad);
        }

        // Assign the remainders
        for (int i = 0; i < remainderLoad; i++) {
            increment(i);
        }
        log.debug(this);

        return capacity;
    }
}
