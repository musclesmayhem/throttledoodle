package doodle.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Karin on 1/29/2016.
 */

@Configuration
@ConfigurationProperties("loadBalance")
@Data
public class LoadBalancerProperties {
    private Integer threadCapacity;
    private Float priorityStoreAllowance;
    private Integer totalThreads;
}
