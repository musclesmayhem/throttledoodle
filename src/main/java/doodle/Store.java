package doodle;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

/**
 * Created by Karin on 1/25/2016.
 */
@ToString(includeFieldNames = true)
public class Store implements Comparable {

    // Store name
    @Getter
    @Setter
    private String name;

    // Assigned when the sequence is processing this store
    @Getter
    private TreeSet<String> messages;

    @Getter
    private Integer totalMessages = 0;

    public Store(String name) {
        this.setName(name);
        this.messages = new TreeSet<String>();
    }

    public void addMessage(String message) {
        messages.add(message);
        totalMessages = messages.size();
    }

    public String grabAMessage(){
        String message = messages.pollFirst();
        totalMessages = messages.size();
        return message;
    }

    public void setMessages(TreeSet<String> messages) {
        this.messages = messages;
        totalMessages = this.messages.size();
    }

    @Override
    public int compareTo(Object o) {

        // Order store in descending order naturally
        if (this.totalMessages == ((Store) o).totalMessages && this.getName().equals(((Store) o).getName()))
            return 0;
        else if (this.totalMessages <= ((Store) o).totalMessages)
            return 1;
        else
            return -1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Store store = (Store) o;

        return (name.equals(store.name) && (totalMessages == store.totalMessages));

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + totalMessages.hashCode();

        return result;
    }
}
