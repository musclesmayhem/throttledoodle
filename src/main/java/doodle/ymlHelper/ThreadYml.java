package doodle.ymlHelper;

import doodle.xmlhelper.SlotXml;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
public class ThreadYml {
    Integer sequence;
    Integer totalMessages;

    List<SlotYml> slot = new ArrayList<SlotYml>();

}
