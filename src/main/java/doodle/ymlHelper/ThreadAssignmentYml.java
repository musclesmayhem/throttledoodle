package doodle.ymlHelper;


import doodle.xmlhelper.ThreadXml;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
public class ThreadAssignmentYml {

    private List<ThreadYml> thread = new ArrayList<ThreadYml>();
    Integer totalMessages;
    Integer totalThreads;

}
