package doodle.ymlHelper;

import doodle.Spin;
import doodle.Store;
import doodle.ThreadSlot;
import doodle.configuration.LoadBalancerProperties;
import doodle.upstream.ConversionRequest;
import doodle.upstream.Initiative;
import doodle.upstream.InitiativeValidator;
import doodle.upstream.Prescriber;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.*;

/**
 * Created by Karin on 1/29/2016.
 */
@Component
@Data
@Log4j2
public class YmlMarshaller {

    private ThreadAssignmentYml assignmentYml;

    @Autowired
    LoadBalancerProperties props;
    @Autowired
    Spin spinner;
    @Autowired
    InitiativeValidator validator;

    public TreeSet<Prescriber> assignTestDrs(String inputFile) {
        InputStream input = null;
        try {
            input = new FileInputStream(new File(inputFile));

            Yaml yaml = new Yaml();
            ArrayList<Prescriber> data = (ArrayList<Prescriber>) yaml.load(input);
            TreeSet<Prescriber> drs = new TreeSet<Prescriber>();

            for (Prescriber p : data)
                drs.add(p);

            return drs;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public TreeSet<Initiative> assignTestInitiatives(String inputFile) {
        InputStream input = null;
        try {
            input = new FileInputStream(new File(inputFile));

            Yaml yaml = new Yaml();
            ArrayList<Initiative> data = (ArrayList<Initiative>) yaml.load(input);
            TreeSet<Initiative> initiatives = new TreeSet<Initiative>();

            for (Initiative p : data)
                initiatives.add(p);

            return initiatives;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void makeTestInitiatives() {
        ArrayList<Initiative> testInitatives = new ArrayList<Initiative>();

        testInitatives.add(new Initiative("Initiative 2"));

        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(testInitatives, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("testInitiativesExample.yml")));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void makeTestDrs() {
        ArrayList<Prescriber> testDrs = new ArrayList<Prescriber>();

        testDrs.add(new Prescriber("Dr. B"));
        testDrs.add(new Prescriber("Dr. C"));
        testDrs.add(new Prescriber("Dr. G"));

        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(testDrs, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("testDrsExample.yml")));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void makeTestQueries() {
        // Test Queries
        Map<Integer, List<ConversionRequest>> testQueries = new HashMap<Integer, List<ConversionRequest>>();
        testQueries.put(0, validator.generateIteration0());
        testQueries.put(1, validator.generateIteration1());

        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(testQueries, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("testQueriesExample.yml")));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<Integer, List<ConversionRequest>> assignTestQueries(String inputFile) {
        InputStream input = null;
        try {
            input = new FileInputStream(new File(inputFile));

            Yaml yaml = new Yaml();
            Map<Integer, List<ConversionRequest>> data = (Map<Integer, List<ConversionRequest>>) yaml.load(input);

            return data;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void makeStoreThrottleFile() {
        Map<String, Integer> storeThrottles = new HashMap<String, Integer>();
        storeThrottles.put("LS-1", 2);
        storeThrottles.put("LS-2", 3);
        storeThrottles.put("LS-3", 3);
        storeThrottles.put("LS-4", 3);

        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(storeThrottles, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("storeThrottleExample.yml")));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Integer> assignStoreThrottle(String inputFile) {
        InputStream input = null;
        try {
            input = new FileInputStream(new File(inputFile));

            Yaml yaml = new Yaml();
            Map<String, Integer> data = (Map<String, Integer>) yaml.load(input);

            return data;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void generateStoreLoad(TreeSet<Store> storeLoad) {

        ArrayList<Store> list = new ArrayList<Store>();

        for (Store s : storeLoad)
            list.add(s);

        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(list, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("storeLoadResult.yml")));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void makeExampleFile() {
        LoadYml yml = new LoadYml();
        TreeSet<Store> set = spinner.initializeTestData1();
        List<StoreYml> storeList = new ArrayList<StoreYml>();

        for (Store s : set) {
            StoreYml store = new StoreYml();
            store.setName(s.getName());
            for (String m : s.getMessages()) {
                store.addMessage(m);
            }
            storeList.add(store);
        }
        yml.setStore(storeList);

        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(yml, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("storeLoadExample.yml")));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TreeSet<Store> assignStoreLoad(String inputFile) {
        InputStream input = null;
        try {
            input = new FileInputStream(new File(inputFile));

            Yaml yaml = new Yaml();
            LoadYml data = (LoadYml) yaml.load(input);

            TreeSet<Store> storeLoad = new TreeSet<Store>();

            for (StoreYml s : data.getStore()) {
                Store store = new Store(s.getName());
                for (String m : s.getMessage()) {
                    store.addMessage(m);
                }
                storeLoad.add(store);
            }
            // Check Store prioritization
            for (Object aStoreLoad : storeLoad) {
                log.debug("Store {}", aStoreLoad.toString());
            }
            return storeLoad;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void applyProperties(String inputFile) {
        InputStream input = null;
        try {
            input = new FileInputStream(new File(inputFile));

            Yaml yaml = new Yaml();
            LoadBalancerProperties data = (LoadBalancerProperties) yaml.load(input);

            props.setThreadCapacity(data.getThreadCapacity());
            props.setTotalThreads(data.getTotalThreads());
            props.setPriorityStoreAllowance(data.getPriorityStoreAllowance());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public String generateYml(TreeMap<Integer, TreeSet<ThreadSlot>> assignment, String fileName) {

        map(assignment);
        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(assignmentYml, writer);

        try {

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));
            out.println(writer.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private void map(TreeMap<Integer, TreeSet<ThreadSlot>> assignment) {
        Integer queuedMessages = 0;
        assignmentYml = new ThreadAssignmentYml();
        assignmentYml.setTotalThreads(assignment.size());
        List<ThreadYml> threads = new ArrayList<ThreadYml>();

        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : assignment.entrySet()) {
            ThreadYml threadYml = new ThreadYml();
            threadYml.setSequence(thread.getKey());

            List<SlotYml> slots = new ArrayList<SlotYml>();
            Integer threadMessages = 0;

            for (ThreadSlot t : thread.getValue()) {
                SlotYml slot = new SlotYml();
                slot.setPriority(t.getPriority());
                slot.setCapacity(t.getCapacity());
                Integer slotMessages = 0;

                List<StoreYml> stores = new ArrayList<StoreYml>();

                for (Store s : t.getStores()) {
                    StoreYml store = new StoreYml();
                    store.setName(s.getName());
                    store.setTotalMessages(s.getTotalMessages());
                    slotMessages += s.getTotalMessages();

                    for (String m : s.getMessages()) {
                        store.addMessage(m);
                    }
                    stores.add(store);
                }
                slot.setStore(stores);
                slot.setTotalMessages(slotMessages);
                slots.add(slot);
                threadMessages += slotMessages;
            }
            threadYml.setTotalMessages(threadMessages);
            threadYml.setSlot(slots);
            threads.add(threadYml);
            queuedMessages += threadMessages;
        }
        assignmentYml.setThread(threads);
        assignmentYml.setTotalMessages(queuedMessages);
    }
}
