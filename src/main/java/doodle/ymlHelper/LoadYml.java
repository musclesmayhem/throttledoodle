package doodle.ymlHelper;

import doodle.Store;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by Karin on 1/30/2016.
 */
@Data
public class LoadYml {
    List<StoreYml> store = new ArrayList<StoreYml>();
}
