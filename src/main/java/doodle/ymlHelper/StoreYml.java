package doodle.ymlHelper;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
public class StoreYml {
    String name;
    Integer totalMessages;

    List<String> message = new ArrayList<String>();

    public void addMessage(String message) {
        this.message.add(message);
        totalMessages = this.message.size();
    }
}
