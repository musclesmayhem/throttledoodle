package doodle.ymlHelper;

import doodle.xmlhelper.StoreXml;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
public class SlotYml {

    Integer priority;
    Integer capacity;
    Integer totalMessages;

    List<StoreYml> store = new ArrayList<StoreYml>();
}
