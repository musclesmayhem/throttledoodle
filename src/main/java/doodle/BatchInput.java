package doodle;

import lombok.Data;

import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by Karin on 2/4/2016.
 */
@Data
public class BatchInput {
    // List of assignedStore and the number of message for the assignedStore
    private TreeSet<Store> storeLoad;

    // This is the resulting data structure for use in Spring Batch Partitioning Step
    private TreeMap<Integer, TreeSet<ThreadSlot>> threadAssignment;

    public BatchInput(TreeSet<Store> load, TreeMap<Integer, TreeSet<ThreadSlot>> assignment){
        storeLoad = load;
        threadAssignment = assignment;
    }
}
