package doodle;

import doodle.configuration.LoadBalancerProperties;
import doodle.upstream.StoreNameComparator;
import doodle.xmlhelper.XmlMarshaller;
import doodle.ymlHelper.YmlMarshaller;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by Karin on 1/25/2016.
 */
@Component
@Log4j2
@Data
public class LoadBalancer {

    @Autowired
    XmlMarshaller xmlMarshaller;

    @Autowired
    YmlMarshaller ymlMarshaller;

    @Autowired
    LoadBalancerProperties loadBalancerProperties;

    // Total number of worker threads
    private Integer totalThreads = 0;

    // Total number of message to be sent
    private Integer totalMessages = 0;

    // Messages Queued so far
    private Integer queuedMessages = 0;

    // List of assignedStore and the number of message for the assignedStore
    private TreeSet<Store> storeLoad;

    private TreeSet<Store> helperSet = new TreeSet<Store>();

    // Total Capacity
    private Integer totalCapacity;

    // Maximum number of message per sequence for this iteration
    private Integer threadCapacity = 0;

    // Allotted space for priority 1 slot, reserved for assignedStore with the largest number of message
    private Float priorityStoreAllowance = 0.0f;

    // The number of slots sharing each sequence capacity. Must be <= threadCapasity
    private Integer threadSlots = 0;

    private SlotCapacity slotCapacity;

    private Integer slotTracker = 0;

    // This is the resulting data structure for use in Spring Batch Partitioning Step
    private TreeMap<Integer, TreeSet<ThreadSlot>> threadAssignment;


    public BatchInput loadBalance(TreeSet<Store> load) {

        storeLoad = load;
        initializeParameters();
        calculateThreadAssignments();

        return new BatchInput(storeLoad, threadAssignment);

    }

    public BatchInput loadBalance(String load) {
        storeLoad = ymlMarshaller.assignStoreLoad(load);
        initializeParameters();
        calculateThreadAssignments();

        return new BatchInput(storeLoad, threadAssignment);

    }

    public BatchInput loadBalance(TreeSet<Store> load, String properties) {

        ymlMarshaller.applyProperties(properties);
        storeLoad = load;
        initializeParameters();
        calculateThreadAssignments();

        return new BatchInput(storeLoad, threadAssignment);

    }

    public BatchInput loadBalance(String load, String properties) {

        ymlMarshaller.applyProperties(properties);
        storeLoad = ymlMarshaller.assignStoreLoad(load);
        initializeParameters();
        calculateThreadAssignments();

        return new BatchInput(storeLoad, threadAssignment);

    }


    private void initializeParameters() {
        slotTracker = 0;
        queuedMessages = 0;
        helperSet = new TreeSet<Store>();
        totalCapacity = 0;

        threadCapacity = loadBalancerProperties.getThreadCapacity();
        priorityStoreAllowance = loadBalancerProperties.getPriorityStoreAllowance();
        totalThreads = loadBalancerProperties.getTotalThreads();
        log.debug("Number of stores at the start of assignment: {}", storeLoad.size());

        totalCapacity = totalThreads * threadCapacity;

        // Prep Thread Assignments
        threadAssignment = new TreeMap();
        for (int thread = 0; thread < totalThreads; thread++) {
            threadAssignment.put(thread, null);
        }
        log.debug("Thread Capacity: {}", threadCapacity);
        log.debug("Priority Store Allowance: {}", priorityStoreAllowance);
        log.debug("Total Threads: {}", totalThreads);
        log.debug("Total Capacity: {}", totalCapacity);
    }

    private void calculateThreadAssignments() {

        // Max ThreadSlots: ceiling(#Stores / #Threads) + 1 (for allowance slot)
        threadSlots = Double.valueOf(Math.ceil(Integer.valueOf(storeLoad.size()).doubleValue() / totalThreads.doubleValue())).intValue() + 1;
        log.debug("Thread Slots: {}", threadSlots);

        totalMessages = 0;
        for (Store s : storeLoad) {
            totalMessages += s.getTotalMessages();
        }

        log.debug("Total number of messages: {}", totalMessages);

        Integer messagesPerThread = totalMessages / totalThreads;
        log.debug("Requested message Load per thread: {}", messagesPerThread);

        if (totalThreads >= totalMessages) {
            // If total threads is greater than total message, assign 1 message to each sequence
            easyAsPie();
        } else if (totalCapacity >= totalMessages && messagesPerThread < threadSlots) {
            // assign 1 message per slot
            fetchTheBoaConstrictor();
        } else if (totalCapacity >= totalMessages && messagesPerThread >= threadSlots) {
            performJumpingJacks();
        } else if (totalCapacity < totalMessages) {
            iDoKungFu();
        }

        // Generate Stores as Map
        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : threadAssignment.entrySet()) {
            for (ThreadSlot t : thread.getValue()) {
                t.generateStoresAsMap();
            }
        }
        check();
    }

    private void easyAsPie() {

        // Reset number of slots per sequence to 1 for (not so) simple assignment
        log.debug("Easy As Pie");
        threadCapacity = 1;
        threadSlots = 1;
        slotCapacity = new SlotCapacity(threadSlots);
        slotCapacity.assign(threadCapacity / threadSlots);

        // Assign slots to the each sequence
        generateThreadSlots();

        // We expect only 1 pass through the assignment
        boolean useAllowance = false;
        assignWork(useAllowance);
    }

    private void iDoKungFu() {
        log.debug("I Do Kung Fu");
        slotCapacity = new SlotCapacity(threadSlots);
        slotCapacity.assignWithAllowance(threadCapacity, priorityStoreAllowance);
        queuedMessages = 0;

        // Assign slots to the each sequence
        generateThreadSlots();

        // Assign message up to capacity
        boolean useAllowance = true;
        assignToCapacity(useAllowance);

        // Return the remaining storeLoad for next iteration
        if (!helperSet.isEmpty()) {
            // Reset the list
            for (Store s : helperSet) {
                storeLoad.add(s);

            }
        }
    }

    private void performJumpingJacks() {

        log.debug("perform Jumping Jacks");

        // Adjust thread capacity
        // Take the floor as baseline, but may need to add one for remainders
        log.debug("Original thread capacity is: {}", threadCapacity);
        threadCapacity = totalMessages / totalThreads;
        log.debug("Adjusted thread capacity is: {}", threadCapacity);

        goFigure();
    }

    private void fetchTheBoaConstrictor() {

        log.debug("Fetch the Boa Constrictor");

        // Adjust thread capacity
        // Take the floor as baseline, but may need to add one for remainders
        log.debug("Original thread capacity is: {}", threadCapacity);
        threadCapacity = totalMessages / totalThreads;
        log.debug("Adjusted thread capacity is: {}", threadCapacity);

        // Adjust sequence slots
        log.debug("Original thread slot is: {}", threadSlots);
        threadSlots = threadCapacity;
        log.debug("Adjusted thread slot is: {}", threadSlots);

        goFigure();
    }

    private void goFigure() {

        slotCapacity = new SlotCapacity(threadSlots);
        slotCapacity.assign(threadCapacity / threadSlots);

        // Assign slot capacity, later, need to add 1 to slots in priority order to accommodate remainder message
        generateThreadSlots();

        recalculateTotalCapacity();

        // Assign message
        boolean useAllowance = false;
        while (queuedMessages < totalMessages) {
            if (assignToCapacity(useAllowance)) return;

            // Add capacity to slots in priority order to accommodate remainder message
            slotCapacity.increment(slotTracker);
            slotTracker++;
            for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : threadAssignment.entrySet()) {
                for (ThreadSlot t : thread.getValue()) {
                    t.setCapacity(slotCapacity.get(t.getPriority()));
                }
            }
            recalculateTotalCapacity();
        }
    }

    private void recalculateTotalCapacity() {
        log.debug("Total Capacity: {}", totalCapacity);
        totalCapacity = 0;

        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : threadAssignment.entrySet()) {
            for (ThreadSlot t : thread.getValue()) {
                totalCapacity += t.getCapacity();
            }
        }
        log.debug("Adjusted total Capacity: {}", totalCapacity);
    }

    private boolean assignToCapacity(boolean useAllowance) {
        while (queuedMessages < totalCapacity) {
            if (assignWork(useAllowance) || storeLoad.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private boolean assignWork(boolean useAllowance) {
        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : threadAssignment.entrySet()) {
            ThreadSlot allowanceSlot = null;

            if (useAllowance) {

                // remove last slot from the sequence for allowance slot
                allowanceSlot = thread.getValue().pollLast();

                // Assign assignedStore with the biggest load to the last slot that holds the allowances
                assignAllowance(allowanceSlot);
            }

            for (ThreadSlot slot : thread.getValue()) {
                if (!storeLoad.isEmpty()) {
                    //Assign
                    assignStoreToSlot(slot);
                } else if (!helperSet.isEmpty()) {
                    // Reset the list
                    for (Store s : helperSet) {
                        storeLoad.add(s);
                    }
                    helperSet.clear();

                    //Assign
                    assignStoreToSlot(slot);
                } else {
                    //Done!
                    log.debug("Done!");
                    return true;
                }
            }
            if (useAllowance) {
                // Put the allowance slot back in the sequence
                thread.getValue().add(allowanceSlot);
            }
        }
        // Reset Store Load
        if(!helperSet.isEmpty()){
            // Reset the list
            for (Store s : helperSet) {
                storeLoad.add(s);
            }
            helperSet.clear();
        }

        return false;
    }

    private void assignAllowance(ThreadSlot allowanceSlot) {
        Store loadStore = storeLoad.pollFirst();
        Store altStore = helperSet.pollFirst();

        // Compare To sorts in descending order naturally for Store
        if (loadStore == null && altStore == null) {
            return;

        } else if (loadStore == null) {
            addMessageToSlot(altStore, allowanceSlot);
        } else if (altStore == null) {
            addMessageToSlot(loadStore, allowanceSlot);
        } else if (altStore.compareTo(loadStore) < 0) {
            addMessageToSlot(altStore, allowanceSlot);
        } else {
            addMessageToSlot(loadStore, allowanceSlot);
        }
        // Add it back to the Set for sorting
        if (altStore != null){
            helperSet.add(altStore);
        }

        if (loadStore != null){
            storeLoad.add(loadStore);
        }

    }

    private void assignStoreToSlot(ThreadSlot slot) {

        Store s = storeLoad.pollFirst();
        addMessageToSlot(s, slot);

        if (s.getTotalMessages() > 0)
            helperSet.add(s);
    }

    private void addMessageToSlot(Store assignedStore, ThreadSlot slot) {

        int totalMessages = 0;
        for (Store dispatchedStore : slot.getStores()) {
            totalMessages += dispatchedStore.getTotalMessages();
        }

        int freeSpace = slot.getCapacity() - totalMessages;

        for (int i = 0; i < freeSpace; i++) {
            if (assignedStore.getTotalMessages() < 1) {
                // All messages for this store have been dispatched
                return;
            }

            String message = assignedStore.grabAMessage();

            // Append if assignedStore already in the list
            for (Store dispatchedStore : slot.getStores()) {
                if (dispatchedStore.getName().equals(assignedStore.getName())) {
                    dispatchedStore.addMessage(message);

                    queuedMessages++;
                    return;
                }
            }
            // Store Not in ThreadSlot
            Store dispatchedStore = new Store(assignedStore.getName());
            dispatchedStore.addMessage(message);
            queuedMessages++;

            slot.addStore(dispatchedStore);
        }
    }

    private void generateThreadSlots() {
        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : threadAssignment.entrySet()) {
            TreeSet<ThreadSlot> slots = new TreeSet<ThreadSlot>();

            for (int priority = 0; priority < threadSlots; priority++) {
                ThreadSlot slot = new ThreadSlot(priority);
                slot.setCapacity(slotCapacity.get(priority));
                slots.add(slot);
            }
            thread.setValue(slots);
        }
    }

    private void check() {
        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : threadAssignment.entrySet()) {
            log.debug("Thread: {}", thread.getKey());
            for (ThreadSlot slot : thread.getValue()) {
                log.debug("Thread Slot: {}", slot);
            }
        }
        log.debug("Remaining Stores at the end of assignment: {}", storeLoad.size());
    }

    public String logAssignmentAsXml(String fileName) {
        return xmlMarshaller.generateXml(threadAssignment, fileName);
    }

    public String logAssignmentAsYml(String fileName) {
        return ymlMarshaller.generateYml(threadAssignment, fileName);
    }
}
