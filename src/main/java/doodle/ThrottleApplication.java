package doodle;

import doodle.upstream.InitiativeValidator;
import doodle.ymlHelper.YmlMarshaller;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

@SpringBootApplication
@Log4j2
public class ThrottleApplication {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(ThrottleApplication.class, args);
//		context.getBean(YmlMarshaller.class).makeExampleFile();
//        context.getBean(YmlMarshaller.class).makeStoreThrottleFile();
//        context.getBean(YmlMarshaller.class).makeTestQueries();
//        context.getBean(YmlMarshaller.class).makeTestDrs();
//        context.getBean(YmlMarshaller.class).makeTestInitiatives();
//        validateInitiatives(args, context);

        loadBalance(args, context);

    }

    private static void validateInitiatives(String[] args, ApplicationContext context) {
        InitiativeValidator initiativeValidator = context.getBean(InitiativeValidator.class);

        // args[0]: Store Throttle Values
        // args[1]: Query Result Set
        // args[2]: Test Bad Prescribers
        // args[3]: Test Bad Initiatives
        // arg parameters: C:\Users\Karin\IdeaProjects\throttle\src\main\resources\storeThrottleExample.yml C:\Users\Karin\IdeaProjects\throttle\src\main\resources\testQueriesExample.yml C:\Users\Karin\IdeaProjects\throttle\src\main\resources\testDrsExample.yml C:\Users\Karin\IdeaProjects\throttle\src\main\resources\testInitiativesExample.yml
        log.debug("args length: {}", args.length);
        for (String s : args) {
            log.debug(s);
        }

        if (args.length >= 4) {
            initiativeValidator.prepareStoreLoad(args[0], args[1], args[2], args[3], 13, 24);
        } else {
            Map<String, Integer> storeThrottles = new HashMap<String, Integer>();
            storeThrottles.put("LS-1", 2);
            storeThrottles.put("LS-2", 3);
            storeThrottles.put("LS-3", 3);
            storeThrottles.put("LS-4", 3);
            initiativeValidator.prepareStoreLoad(storeThrottles, 13, 24);
        }
    }

    public static void loadBalance(String[] args, ApplicationContext context) {
        LoadBalancer loadBalancer = context.getBean(LoadBalancer.class);

        // args[0]: Load File
        // args[1]: properties file
        // arg parameters: C:\Users\Karin\IdeaProjects\throttle\src\main\resources\storeLoad.yml C:\Users\Karin\IdeaProjects\throttle\src\main\resources\config.yml
        log.debug("args length: {}", args.length);
        for (String s : args) {
            log.debug(s);
        }
        Integer fileNumber = 0;
        if (args.length >= 2) {
            BatchInput assignment = loadBalancer.loadBalance(args[0], args[1]);
            print(loadBalancer, fileNumber);
            fileNumber++;

            while (assignment.getStoreLoad().size() > 0) {
                assignment.setStoreLoad((loadBalancer.loadBalance(assignment.getStoreLoad())).getStoreLoad());
                print(loadBalancer, fileNumber);
                fileNumber++;
            }
        }
        // No user input, make up some test data to run
        else {
            Spin spinner = context.getBean(Spin.class);
//            TreeSet<Store> assignment1 = spinner.initializeTestData1();
            TreeSet<Store> assignment1 = spinner.initializeTestData2();
            BatchInput input = loadBalancer.loadBalance(assignment1);
            print(loadBalancer, fileNumber);
            fileNumber++;

            while (input.getStoreLoad().size() > 0) {
                input.setStoreLoad((loadBalancer.loadBalance(input.getStoreLoad())).getStoreLoad());
                print(loadBalancer, fileNumber);
                fileNumber++;
            }

        }
    }

    private static void print(LoadBalancer loadBalancer, Integer number) {
        loadBalancer.logAssignmentAsYml(String.format("ThreadAssignment-%d.yml", number));
//        loadBalancer.logAssignmentAsXml(String.format("ThreadAssignment-%d.xml", number));
    }
}
