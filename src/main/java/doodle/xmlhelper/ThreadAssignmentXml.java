package doodle.xmlhelper;


import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ThreadAssignmentXml {

    Integer totalMessages;
    Integer totalThreads;

    @XmlElementWrapper(name = "threads")
    private List<ThreadXml> thread = new ArrayList<ThreadXml>();

}
