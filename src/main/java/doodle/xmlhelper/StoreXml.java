package doodle.xmlhelper;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class StoreXml {
    String name;
    Integer totalMessages;

    @XmlElementWrapper(name = "messages")
    List<String> message = new ArrayList<String>();

    public void addMessage(String message) {
        this.message.add(message);
        totalMessages = this.message.size();
    }
}
