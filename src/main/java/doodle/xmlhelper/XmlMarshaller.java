package doodle.xmlhelper;

import doodle.Store;
import doodle.ThreadSlot;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by Karin on 1/29/2016.
 */
@Component
@Data
@Log4j2
public class XmlMarshaller {

    private ThreadAssignmentXml assignmentXml;

    public String generateXml(TreeMap<Integer, TreeSet<ThreadSlot>> assignment, String fileName) {

        map(assignment);
        StringWriter writer = new StringWriter();

        try {
            JAXBContext context = JAXBContext.newInstance(ThreadAssignmentXml.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(assignmentXml, writer);

            // Marshal to file
            marshaller.marshal(assignmentXml, new File(fileName));

            return writer.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private void map(TreeMap<Integer, TreeSet<ThreadSlot>> assignment) {
        Integer queuedMessages = 0;
        assignmentXml = new ThreadAssignmentXml();
        assignmentXml.setTotalThreads(assignment.size());
        List<ThreadXml> threads = new ArrayList<ThreadXml>();

        for (Map.Entry<Integer, TreeSet<ThreadSlot>> thread : assignment.entrySet()) {
            ThreadXml threadXml = new ThreadXml();
            threadXml.setSequence(thread.getKey());

            List<SlotXml> slots = new ArrayList<SlotXml>();
            Integer threadMessages = 0;

            for (ThreadSlot t : thread.getValue()) {
                SlotXml slot = new SlotXml();
                slot.setPriority(t.getPriority());
                slot.setCapacity(t.getCapacity());
                Integer slotMessages = 0;

                List<StoreXml> stores = new ArrayList<StoreXml>();

                for (Store s : t.getStores()) {
                    StoreXml store = new StoreXml();
                    store.setName(s.getName());
                    store.setTotalMessages(s.getTotalMessages());
                    slotMessages += s.getTotalMessages();

                    for (String m : s.getMessages()) {
                        store.addMessage(m);
                    }
                    stores.add(store);
                }
                slot.setStore(stores);
                slot.setTotalMessages(slotMessages);
                slots.add(slot);
                threadMessages += slotMessages;
            }
            threadXml.setTotalMessages(threadMessages);
            threadXml.setSlot(slots);
            threads.add(threadXml);
            queuedMessages += threadMessages;
        }
        assignmentXml.setThread(threads);
        assignmentXml.setTotalMessages(queuedMessages);
    }
}
