package doodle.xmlhelper;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karin on 1/29/2016.
 */
@Data
@XmlType(propOrder = {"sequence", "totalMessages", "slot"})
@XmlAccessorType(XmlAccessType.FIELD)
public class ThreadXml {
    Integer sequence;
    Integer totalMessages;

    @XmlElementWrapper(name = "slots")
    List<SlotXml> slot = new ArrayList<SlotXml>();

}
