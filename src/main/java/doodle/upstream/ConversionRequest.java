package doodle.upstream;

import lombok.Data;

/**
 * Created by Karin on 2/7/2016.
 */
@Data
public class ConversionRequest {
    String initiative;
    String prescriber;
    String store;
    String ConvId;
}
