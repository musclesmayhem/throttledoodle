package doodle.upstream;

import doodle.Store;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.TreeSet;

/**
 * Created by Karin on 2/7/2016.
 */
@ToString(includeFieldNames = true)
public class Prescriber implements Comparable<Prescriber>{
    @Getter
    @Setter
    String provDtlId;

    // Store list sorted by name
    @Getter @Setter
    TreeSet<Store> stores = new TreeSet<Store>(new StoreNameComparator());

    public Prescriber(){}

    public Prescriber(String id) {
        provDtlId = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Prescriber that = (Prescriber) o;

        return getProvDtlId().equals(that.getProvDtlId());

    }

    @Override
    public int hashCode() {
        return getProvDtlId().hashCode();
    }

    @Override
    public int compareTo(Prescriber o) {
        return this.getProvDtlId().compareTo(o.getProvDtlId());
    }
}
