package doodle.upstream;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Karin on 2/5/2016.
 */
@ToString
public class StoreThrottle {
    String name;
    Integer priority;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoreThrottle that = (StoreThrottle) o;

        if (!name.equals(that.name)) return false;
        return priority.equals(that.priority);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + priority.hashCode();
        return result;
    }
}
