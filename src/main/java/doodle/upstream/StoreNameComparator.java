package doodle.upstream;

import doodle.Store;
import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * Created by Karin on 2/7/2016.
 */
@Component
public class StoreNameComparator implements Comparator<Store> {
    @Override
    public int compare(Store o1, Store o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
