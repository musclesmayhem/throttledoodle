package doodle.upstream;

import doodle.Store;
import doodle.ymlHelper.YmlMarshaller;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Karin on 2/5/2016.
 */
@Component
@Data
@Log4j2
public class InitiativeValidator {

    @Autowired
    StoreNameComparator storeNameComparator;

    @Autowired
    YmlMarshaller ymlMarshaller;

    // Input from throttling component
    private Map<String, Integer> storeThrottles = new HashMap<String, Integer>();

    // Keeper of the Good Initiatives
    private TreeSet<Initiative> goodOnes = new TreeSet<Initiative>();

    // Tracker of Good Prescribers
    private TreeSet<Prescriber> goodDrs;

    private TreeSet<Initiative> wip = new TreeSet<Initiative>();

    //  Tracker for the stores needing back-filling
    private HashSet<String> backFill = new HashSet<String>();

    // Output for Load Balancing
    private TreeSet<Store> storeLoad = new TreeSet<Store>();

    // Test data iteration to simulate db pull
    private Integer dbPulls = 0;

    // Test Queries
    private Map<Integer, List<ConversionRequest>> testQueries = new HashMap<Integer, List<ConversionRequest>>();

    //Test Bad Drs
    private TreeSet<Prescriber> badDrs = new TreeSet<Prescriber>();

    //Test Bad Initiatives
    private TreeSet<Initiative> badInitiatives = new TreeSet<Initiative>();

    public TreeSet<Store> prepareStoreLoad(String inputFile, String queries, String drs, String inits, int... status) {
        Map<String, Integer> storeThrottles = ymlMarshaller.assignStoreThrottle(inputFile);
        // Generate test data
        testQueries = ymlMarshaller.assignTestQueries(queries);
        badDrs = ymlMarshaller.assignTestDrs(drs);
        badInitiatives = ymlMarshaller.assignTestInitiatives(inits);

        return prepareStoreLoad(storeThrottles, status);
    }

    public TreeSet<Store> prepareStoreLoad(String inputFile, String queries, String drs, int... status) {
        Map<String, Integer> storeThrottles = ymlMarshaller.assignStoreThrottle(inputFile);
        // Generate test data
        testQueries = ymlMarshaller.assignTestQueries(queries);
        badDrs = ymlMarshaller.assignTestDrs(drs);

        badInitiatives.add(new Initiative("Initiative 2"));

        return prepareStoreLoad(storeThrottles, status);
    }

    public TreeSet<Store> prepareStoreLoad(String inputFile, String queries, int... status) {
        Map<String, Integer> storeThrottles = ymlMarshaller.assignStoreThrottle(inputFile);
        // Generate test data
        testQueries = ymlMarshaller.assignTestQueries(queries);

        badInitiatives.add(new Initiative("Initiative 2"));

        badDrs.add(new Prescriber("Dr. B"));
        badDrs.add(new Prescriber("Dr. C"));
        badDrs.add(new Prescriber("Dr. G"));

        return prepareStoreLoad(storeThrottles, status);
    }

    public TreeSet<Store> prepareStoreLoad(String inputFile, int... status) {
        Map<String, Integer> storeThrottles = ymlMarshaller.assignStoreThrottle(inputFile);
        // Generate test data
        testQueries.put(0, generateIteration0());
        testQueries.put(1, generateIteration1());

        badInitiatives.add(new Initiative("Initiative 2"));

        badDrs.add(new Prescriber("Dr. B"));
        badDrs.add(new Prescriber("Dr. C"));
        badDrs.add(new Prescriber("Dr. G"));

        return prepareStoreLoad(storeThrottles, status);
    }

    public TreeSet<Store> prepareStoreLoad(Map<String, Integer> storeThrottles, int... status) {
        //TODO: Retrieve All records with status of 13 or 24
        //TODO:initiative ID, prescriber ID, Store ID, Conv ID
        //TODO: Up to the number of throttle List
//        for(int i = 0; i <= dbPulls; i++){
        while (!storeThrottles.isEmpty()) {
            log.debug("store throttle values: {}", storeThrottles);
            List<ConversionRequest> results = fetchRequests(storeThrottles, status);
            log.debug("Query result is: {}", results);
            Map<String, Integer> backFillThrottle = new HashMap<String, Integer>();

            if (results != null && !results.isEmpty()) {
                buildWorkStructure(results);

                if (!goodOnes.isEmpty())
                    pruneGoodOnesFromWip();

                validateInitiatives();
                validatePrescribers();

                addToGoodOnes();

                if (!backFill.isEmpty()) {
                    // Some validations have failed.  backfill.
                    for (String s : backFill) {
                        backFillThrottle.put(s, storeThrottles.get(s));
                    }
                }
                log.debug("backfill Throttle: {}", backFillThrottle);
            }
            storeThrottles = backFillThrottle;
            backFill = new HashSet<String>();
        }

        check();
        log.debug("Validated Initiatives: {}", goodOnes);
        return loadStore();
    }

    private TreeSet<Store> loadStore() {
        TreeSet<Store> load = new TreeSet<Store>(storeNameComparator);

        for (Initiative i : goodOnes) {
            for (Prescriber p : i.getPrescribers()) {
                for (Store s : p.getStores()) {
                    Store foundStore = load.ceiling(s);
                    if (foundStore != null && storeNameComparator.compare(foundStore, s) == 0) {
                        //Store already in the list.  Add messages
                        for (String m : s.getMessages()) {
                            foundStore.addMessage(m);
                        }
                    } else {
                        load.add(s);
                    }

                }
            }
        }
        log.debug("Store Load by name is: {}", load);

        for (Store s : load)
            storeLoad.add(s);

        log.debug("Store Load is: {}", storeLoad);
        ymlMarshaller.generateStoreLoad(storeLoad);
        return storeLoad;
    }

    private void pruneGoodOnesFromWip() {

        TreeSet<Initiative> pruneMarker = new TreeSet<Initiative>();
        goodDrs = new TreeSet<Prescriber>();

        //If it's already in the Good Ones, remove from WIP
        log.debug("pruning");
        log.debug("WIP is: {}", wip);
        log.debug("good Initiatives: {}", goodOnes);

        // Build distinct good prescriber list
        for (Initiative init : goodOnes) {
            for (Prescriber dr : init.getPrescribers()) {
                goodDrs.add(dr);
            }
        }

        for (Initiative wipInit : wip) {
            Initiative doneInit = goodOnes.ceiling(wipInit);
            log.debug("Good Initiative found for WIP: {}", doneInit);
            if (doneInit != null && wipInit.equals(doneInit)) {
                //If all prescribers for this inititives are valid, then remove this initiative
                removeValidPrescribers(wipInit, doneInit);
                if (wipInit.getPrescribers().isEmpty()) {
                    // Mark for removal
                    pruneMarker.add(wipInit);
                    log.debug("Removing from WIP initiative {}", wipInit);
                }

            }
        }
        // Prune
        for (Initiative i : pruneMarker)
            wip.remove(i);
    }

    private void removeValidPrescribers(Initiative wipInit, Initiative goodInit) {

        TreeSet<Prescriber> wipDrs = wipInit.getPrescribers();
        TreeSet<Prescriber> doneDrs = goodInit.getPrescribers();
        TreeSet<Prescriber> pruneMarker = new TreeSet<Prescriber>();

        // Remove validated prescriber from WIP
        for (Prescriber goodDr : goodDrs) {
            Prescriber wipDr = wipDrs.ceiling(goodDr);
            if (wipDr != null && wipDr.equals(goodDr)) {
                // Move stores from wipDr to doneDr
                Prescriber doneDr = doneDrs.ceiling(wipDr);
                if (doneDr != null && wipDr.equals(doneDr)) {
                    // Prescriber is already in Done list
                    for (Store wipStore : wipDr.getStores()) {
                        Store doneStore = doneDr.getStores().ceiling(wipStore);
                        if (doneStore != null && storeNameComparator.compare(doneStore, wipStore) == 0) {
                            for (String m : wipStore.getMessages()) {
                                doneStore.addMessage(m);
                            }
                        } else {
                            doneDr.getStores().add(wipStore);
                        }
                    }
                } else {
                    // Add prescriber to Done list
                    goodInit.getPrescribers().add(wipDr);
                }
                // Track for pruning
                pruneMarker.add(wipDr);
                log.debug("Removing from WIP dr {}", wipDr);
            }
        }

        // Prune
        for (Prescriber p : pruneMarker)
            wipDrs.remove(p);
    }

    private void addToGoodOnes() {
        for (Initiative i : wip) {
            Initiative initFound = goodOnes.ceiling(i);
            if (initFound != null && i.equals(initFound)) {
                // Initiative is already stored
                for (Prescriber p : i.getPrescribers()) {
                    Prescriber drFound = initFound.getPrescribers().ceiling(p);
                    if (drFound != null && p.equals(drFound)) {
                        // Dr is already stored
                        for (Store s : p.getStores()) {
                            Store storeFound = drFound.getStores().ceiling(s);
                            if (storeFound == null || storeNameComparator.compare(storeFound, s) != 0) {
                                // Store is not there
                                storeFound = new Store(s.getName());
                                drFound.getStores().add(storeFound);
                            }
                            for (String m : s.getMessages()) {
                                storeFound.addMessage(m);
                            }
                        }
                    } else {
                        initFound.getPrescribers().add(p);
                    }
                }
            } else {
                goodOnes.add(i);
            }

        }
        wip = new TreeSet<Initiative>();
    }

    private void validatePrescribers() {

        TreeSet<Prescriber> prescribers = new TreeSet<Prescriber>();

        // Build distinct prescriber list
        for (Initiative init : wip) {
            for (Prescriber dr : init.getPrescribers()) {
                prescribers.add(dr);
            }
        }
        log.debug("Unique Prescribers list: {}", prescribers);

        // Validate prescribers
        for (Prescriber dr : prescribers) {
            if (!isValid(dr)) {
                for (Initiative init : wip) {
                    TreeSet<Prescriber> initDrs = init.getPrescribers();
                    Prescriber found = initDrs.ceiling(dr);
                    if (found != null && found.equals(dr)) {
                        for (Store s : found.getStores()) {
                            backFill.add(s.getName());
                        }
                        initDrs.remove(found);
                        log.debug("Removed Dr: {}", found);
                    }
                }
            }
        }
    }

    private void validateInitiatives() {
        TreeSet<Initiative> deleteMarker = new TreeSet<Initiative>();

        for (Initiative init : wip) {
            if (!isValid(init)) {
                for (Prescriber p : init.getPrescribers()) {
                    for (Store s : p.getStores()) {
                        backFill.add(s.getName());
                    }
                }
                // Mark for deletion
                deleteMarker.add(init);
                log.debug("Removed initiative: {}", init);
            }
        }

        // Remove initiatives from the list
        for (Initiative i : deleteMarker) {
            wip.remove(i);
        }
    }

    private Boolean isValid(Prescriber dr) {
        return mockPrescriberValidation(dr);
    }

    //TODO: Replace mockInitiativeValidation with real Initiative Validation
    private Boolean isValid(Initiative initiative) {
        // Call Initiative Validation and return validation status
        return mockInitValidation(initiative);
    }

    private void buildWorkStructure(List<ConversionRequest> results) {
        wip = new TreeSet<Initiative>();
        for (ConversionRequest result : results) {
            log.debug("----------");
            log.debug("Reading: {}", result);
            Prescriber prescriber = new Prescriber(result.getPrescriber());
            Initiative initiative = new Initiative(result.getInitiative());
            Store store = new Store(result.getStore());

            Initiative initFound = wip.ceiling(initiative);
            if (initFound != null && initiative.equals(initFound)) {
                // Initiative is already on the list
                // Search prescriber
                Prescriber prescFound = initFound.getPrescribers().ceiling(prescriber);
                if (prescFound != null && prescriber.equals(prescFound)) {
                    // Prescriber found
                    // search Store
                    Store storeFound = prescFound.getStores().ceiling(store);
                    if (storeFound != null && storeNameComparator.compare(storeFound, store) == 0) {
                        // Store Found.  Add the conversion Request
                        storeFound.addMessage(result.getConvId());
                    } else {
                        // Add the store and conversion request
                        store.addMessage(result.getConvId());
                        prescFound.getStores().add(store);
                    }
                } else {
                    // Add the prescriber and request's store
                    store.addMessage(result.getConvId());
                    prescriber.getStores().add(store);
                    initFound.getPrescribers().add(prescriber);
                }
            } else {
                // Add the new Initiative and request's prescriber
                store.addMessage(result.getConvId());
                prescriber.getStores().add(store);
                initiative.getPrescribers().add(prescriber);
                wip.add(initiative);
            }

        }
        log.debug("Initiatives to process:");
        check();
    }

    private void check() {
        for (Initiative i : wip) {
            log.debug(i);
        }
    }

    //TODO: Replace test data with Query to the database
    private List<ConversionRequest> fetchRequests(Map<String, Integer> storeThrottles, int... status) {
        return generateTestData();
    }

    private Boolean mockPrescriberValidation(Prescriber dr) {
        if (badDrs.contains(dr))
            return false;
        return true;
    }

    private Boolean mockInitValidation(Initiative initiative) {
        if (badInitiatives.contains(initiative))
            return false;
        return true;
    }

    private List<ConversionRequest> generateTestData() {
        List<ConversionRequest> results = testQueries.get(dbPulls);
        dbPulls++;
        return results;
    }

    public List<ConversionRequest> generateIteration1() {
        List<ConversionRequest> testData = new ArrayList<ConversionRequest>();

        ConversionRequest c3 = new ConversionRequest();
        c3.setInitiative("Initiative 1");
        c3.setPrescriber("Dr. A");
        c3.setStore("LS-2");
        c3.setConvId("Request 3");
        testData.add(c3);

        ConversionRequest c4 = new ConversionRequest();
        c4.setInitiative("Initiative 1");
        c4.setPrescriber("Dr. C");
        c4.setStore("LS-2");
        c4.setConvId("Request 6");
        testData.add(c4);

        ConversionRequest c5 = new ConversionRequest();
        c5.setInitiative("Initiative 3");
        c5.setPrescriber("Dr. D");
        c5.setStore("LS-2");
        c5.setConvId("Request 7");
        testData.add(c5);

        ConversionRequest c6 = new ConversionRequest();
        c6.setInitiative("Initiative 4");
        c6.setPrescriber("Dr. E");
        c6.setStore("LS-4");
        c6.setConvId("Request 8");
        testData.add(c6);

        ConversionRequest c7 = new ConversionRequest();
        c7.setInitiative("Initiative 4");
        c7.setPrescriber("Dr. F");
        c7.setStore("LS-4");
        c7.setConvId("Request 9");
        testData.add(c7);

//        ConversionRequest c8 = new ConversionRequest();
//        c8.setInitiative("Initiative 5");
//        c8.setPrescriber("Dr. K");
//        c8.setStore("LS-4");
//        c8.setConvId("Request 11");
//        testData.add(c8);

        ConversionRequest c8 = new ConversionRequest();
        c8.setInitiative("Initiative 5");
        c8.setPrescriber("Dr. E");
        c8.setStore("LS-4");
        c8.setConvId("Request 11");
        testData.add(c8);

        return testData;
    }

    public List<ConversionRequest> generateIteration0() {

        List<ConversionRequest> testData = new ArrayList<ConversionRequest>();

        ConversionRequest c1 = new ConversionRequest();
        c1.setInitiative("Initiative 1");
        c1.setPrescriber("Dr. A");
        c1.setStore("LS-1");
        c1.setConvId("Request 1");
        testData.add(c1);

        ConversionRequest c2 = new ConversionRequest();
        c2.setInitiative("Initiative 1");
        c2.setPrescriber("Dr. A");
        c2.setStore("LS-1");
        c2.setConvId("Request 2");
        testData.add(c2);

        ConversionRequest c3 = new ConversionRequest();
        c3.setInitiative("Initiative 1");
        c3.setPrescriber("Dr. A");
        c3.setStore("LS-2");
        c3.setConvId("Request 3");
        testData.add(c3);

        ConversionRequest c4 = new ConversionRequest();
        c4.setInitiative("Initiative 1");
        c4.setPrescriber("Dr. B");
        c4.setStore("LS-2");
        c4.setConvId("Request 4");
        testData.add(c4);

        ConversionRequest c5 = new ConversionRequest();
        c5.setInitiative("Initiative 2");
        c5.setPrescriber("Dr. B");
        c5.setStore("LS-2");
        c5.setConvId("Request 5");
        testData.add(c5);

        ConversionRequest c6 = new ConversionRequest();
        c6.setInitiative("Initiative 4");
        c6.setPrescriber("Dr. E");
        c6.setStore("LS-4");
        c6.setConvId("Request 8");
        testData.add(c6);

        ConversionRequest c7 = new ConversionRequest();
        c7.setInitiative("Initiative 4");
        c7.setPrescriber("Dr. F");
        c7.setStore("LS-4");
        c7.setConvId("Request 9");
        testData.add(c7);

        ConversionRequest c8 = new ConversionRequest();
        c8.setInitiative("Initiative 5");
        c8.setPrescriber("Dr. G");
        c8.setStore("LS-4");
        c8.setConvId("Request 10");
        testData.add(c8);

        return testData;
    }
}
