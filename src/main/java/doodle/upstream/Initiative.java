package doodle.upstream;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.TreeSet;

/**
 * Created by Karin on 2/5/2016.
 */
@ToString(includeFieldNames = true)
public class Initiative implements Comparable<Initiative>{
    @Getter
    @Setter
    String initId;
    @Getter
    @Setter
    TreeSet<Prescriber> prescribers = new TreeSet<Prescriber>();

    public Initiative(String id) {
        initId = id;
    }
    public Initiative(){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Initiative that = (Initiative) o;

        return getInitId().equals(that.getInitId());

    }

    @Override
    public int hashCode() {
        int result = getInitId().hashCode();
        return result;
    }

    @Override
    public int compareTo(Initiative o) {
        return (this.getInitId().compareTo(o.getInitId()));
    }
}
