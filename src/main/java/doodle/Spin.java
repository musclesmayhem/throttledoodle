package doodle;

import doodle.configuration.LoadBalancerProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.TreeSet;

/**
 * Created by Karin on 1/30/2016.
 */
@Component
@Log4j2
public class Spin {

    public TreeSet<Store> initializeTestData1() {

        TreeSet<Store> storeLoad;

        storeLoad = new TreeSet();

        Store LS1 = new Store("LS1");
        LS1.addMessage("LS1-1");
        storeLoad.add(LS1);

        Store LS2 = new Store("LS2");
        LS2.addMessage("LS2-1");
        storeLoad.add(LS2);

        Store LS3 = new Store("LS3");
        LS3.addMessage("LS3-1");
        LS3.addMessage("LS3-2");
        storeLoad.add(LS3);

        Store LS4 = new Store("LS4");
        LS4.addMessage("LS4-1");
        storeLoad.add(LS4);

        Store MO = new Store("MO");
        MO.addMessage("MO-1");
        MO.addMessage("MO-2");
        MO.addMessage("MO-3");
        MO.addMessage("MO-4");
        MO.addMessage("MO-5");
        MO.addMessage("MO-6");
        MO.addMessage("MO-7");
        MO.addMessage("MO-8");
        MO.addMessage("MO-9");
//        MO.addMessage("MO-10");
        storeLoad.add(MO);

        Store LS5 = new Store("LS5");
        LS5.addMessage("LS5-1");
        storeLoad.add(LS5);

        Store LS6 = new Store("LS6");
        LS6.addMessage("LS6-1");
        LS6.addMessage("LS6-2");
        storeLoad.add(LS6);

        Store LS7 = new Store("LS7");
        LS7.addMessage("LS7-1");
        storeLoad.add(LS7);

        Store LS8 = new Store("LS8");
        LS8.addMessage("LS8-1");
        storeLoad.add(LS8);

        log.debug("Assignment 1");

//        Check Store prioritization
        for (Object aStoreLoad : storeLoad) {
            log.debug("Store {}", aStoreLoad.toString());
        }

        return storeLoad;

    }

    public TreeSet<Store> initializeTestData2() {
        log.debug("Initializing for Large Load...");
        TreeSet<Store> storeLoad;

        storeLoad = new TreeSet();

        Store LS1 = new Store("LS1");
        LS1.addMessage("LS1-1");
        storeLoad.add(LS1);

        Store LS2 = new Store("LS2");
        LS2.addMessage("LS2-1");
        storeLoad.add(LS2);

        Store LS3 = new Store("LS3");
        LS3.addMessage("LS3-1");
        LS3.addMessage("LS3-2");
        storeLoad.add(LS3);

        Store LS4 = new Store("LS4");
        LS4.addMessage("LS4-1");
        storeLoad.add(LS4);

        Store MO = new Store("MO");
        MO.addMessage("MO-1");
        MO.addMessage("MO-2");
        MO.addMessage("MO-3");
        MO.addMessage("MO-4");
        MO.addMessage("MO-5");
        MO.addMessage("MO-6");
        MO.addMessage("MO-7");
        MO.addMessage("MO-8");
        MO.addMessage("MO-9");
        MO.addMessage("MO-10");
        storeLoad.add(MO);

        Store LS5 = new Store("LS5");
        LS5.addMessage("LS5-1");
        storeLoad.add(LS5);

        Store LS6 = new Store("LS6");
        LS6.addMessage("LS6-1");
        LS6.addMessage("LS6-2");
        storeLoad.add(LS6);

        Store LS7 = new Store("LS7");
        LS7.addMessage("LS7-1");
        storeLoad.add(LS7);

        Store LS8 = new Store("LS8");
        LS8.addMessage("LS8-1");
        storeLoad.add(LS8);

        curnOutStores(storeLoad);

        log.debug("Assignment 2");

//        Check Store prioritization
        for (Object aStoreLoad : storeLoad) {
            log.debug("Store {}", aStoreLoad.toString());
        }

        return storeLoad;
    }

    private void curnOutStores(TreeSet<Store> storeLoad) {
        String storeNameTemplate = "Store";
        Integer msgPerStore = 500;
        Integer numberOfStores = 1000;

        for (int i = 0; i < numberOfStores; i++){
            String storeName = storeNameTemplate+i;
            Store newStore = new Store(storeName);
            for(int j = 0; j < msgPerStore; j++){
                newStore.addMessage(storeName+"-"+j);
            }
            storeLoad.add(newStore);
        }
    }

    //    private void initializeTestData4() {
//        threadCapacity = 20;
//        priorityStoreAllowance = .1f;
//        threadSlots = 5;
//        totalThreads = 5;
//        slotTracker = 0;
//
//        // Prep Thread Assignments
//        threadAssignment = new TreeMap();
//        for (int sequence = 0; sequence < totalThreads; sequence++) {
//            threadAssignment.put(sequence, null);
//        }
//
//        storeLoad = new TreeSet();
//
//        storeLoad.add(new Store("LS1", 1));
//        storeLoad.add(new Store("LS2", 5));
//        storeLoad.add(new Store("LS3", 10));
//        storeLoad.add(new Store("LS4", 1));
//        storeLoad.add(new Store("MO", 50));
//        storeLoad.add(new Store("LS5", 5));
//        storeLoad.add(new Store("LS6", 20));
//        storeLoad.add(new Store("LS7", 1));
//        storeLoad.add(new Store("LS8", 10));
//
//        //Check Store prioritization
//        log.debug("Assignment 4");
////        for (Object aStoreLoad : storeLoad) {
////            log.debug("Store {}", aStoreLoad.toString());
////        }
//    }

//    private void initializeTestData3() {
//        threadCapacity = 5;
//        priorityStoreAllowance = .2f;
//        threadSlots = 3;
//        totalThreads = 5;
//        slotTracker = 0;
//
//        // Prep Thread Assignments
//        threadAssignment = new TreeMap();
//        for (int sequence = 0; sequence < totalThreads; sequence++) {
//            threadAssignment.put(sequence, null);
//        }
//
//        storeLoad = new TreeSet();
//
//        storeLoad.add(new Store("LS1", 1));
//        storeLoad.add(new Store("LS2", 1));
//        storeLoad.add(new Store("LS3", 2));
//        storeLoad.add(new Store("LS4", 1));
//        storeLoad.add(new Store("MO", 10));
//        storeLoad.add(new Store("LS5", 1));
//        storeLoad.add(new Store("LS6", 3));
//        storeLoad.add(new Store("LS7", 1));
//        storeLoad.add(new Store("LS8", 2));
//
//        //Check Store prioritization
//        log.debug("Assignment 3");
////        for (Object aStoreLoad : storeLoad) {
//            log.debug("Store {}", aStoreLoad.toString());
//        }
//    }

//    private void initializeTestData2() {
//        threadCapacity = 5;
//        priorityStoreAllowance = .2f;
//        threadSlots = 5;
//        totalThreads = 5;
//        slotTracker = 0;
//
//        // Prep Thread Assignments
//        threadAssignment = new TreeMap();
//        for (int sequence = 0; sequence < totalThreads; sequence++) {
//            threadAssignment.put(sequence, null);
//        }
//
//        storeLoad = new TreeSet();
//
//        storeLoad.add(new Store("LS1", 1));
//        storeLoad.add(new Store("LS2", 1));
//        storeLoad.add(new Store("LS3", 2));
//        storeLoad.add(new Store("LS4", 1));
//        storeLoad.add(new Store("MO", 10));
//        storeLoad.add(new Store("LS5", 1));
//        storeLoad.add(new Store("LS6", 3));
//        storeLoad.add(new Store("LS7", 1));
//        storeLoad.add(new Store("LS8", 2));
//
//        //Check Store prioritization
//        log.debug("Assignment 2");
//        for (Object aStoreLoad : storeLoad) {
//            log.debug("Store {}", aStoreLoad.toString());
//        }
//    }
}
