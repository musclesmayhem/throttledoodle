package doodle;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

/**
 * Created by Karin on 1/27/2016.
 */
@ToString(includeFieldNames = true)
public class ThreadSlot implements Comparable{

    @Getter @Setter
    private Integer priority = 0;

    @Getter @Setter
    private Integer capacity = 0;

    @Getter @Setter
    private Set<Store> stores = new TreeSet<Store>();

    @Getter @Setter
    HashMap<String, List<String>> storeMap;

    public ThreadSlot(Integer p){
        priority = p;
    }

    public void addStore(Store store){
        stores.add(store);
    }

    public HashMap<String, List<String>> generateStoresAsMap(){

        storeMap = new java.util.HashMap();

        for(Store s : stores){
            storeMap.put(s.getName(),new ArrayList<String>(s.getMessages()));
        }
        return storeMap;
    }

    @Override
    public int compareTo(Object o) {
        if (this.priority == ((ThreadSlot) o).getPriority() &&
                this.capacity == ((ThreadSlot) o).getCapacity() &&
                this.stores == ((ThreadSlot) o).getStores())
            return 0;
        else if (this.priority >= ((ThreadSlot) o).getPriority())
            return 1;
        else
            return -1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThreadSlot that = (ThreadSlot) o;

        if (!priority.equals(that.priority)) return false;
        if (!capacity.equals(that.capacity)) return false;
        return stores.equals(that.stores);

    }

    @Override
    public int hashCode() {
        int result = priority.hashCode();
        result = 31 * result + capacity.hashCode();
        result = 31 * result + stores.hashCode();
        return result;
    }
}
