# README #

Welcome to Throttle Doodle README.

### Design Artifacts ###
Design documentation (with pictures) can be downloaded from Confluence eCIMS space:
* http://confluence.ads.kp.org/confluence/download/attachments/12812453/ThrottleLoadBalancing-20160104.pptx?api=v2
* http://confluence.ads.kp.org/confluence/download/attachments/12812453/RC%20Throttling%20-%2020170804.pdf?api=v2
* Design artifacts location: http://confluence.ads.kp.org/confluence/display/ECIMS/eCIMS+Design+Artifacts

### To Test Load Balancer ###
To execute the application in windows:

* Click the Downloads link on the Bitbucket left NAVIGATION menu.
* Click Download repository
* Unzip the downloaded file
* Unzip the application folder located in: <unzip directory>\build\distributions\throttle.zip
* Copy the 2 input files: config.yml and storeLoad.yml from <unzip directory>\src\main\resources to the application location: <unzip directory>\build\distributions\throttle\throttle\bin
* Launch a windows command prompt and navigate to <unzip directory>\build\distributions\throttle\throttle\bin
* Execute the following command: throttle.bat storeLoad.yml config.yml
* The application should produce thread assignment reports in the same directory.
* Validate thread assignments are correct by examining ThrottleAssignment.xml or ThrottleAssignment.yml.

### To Test Initiative and Prescriber Validations ###
To execute the application in windows:

* Click the Downloads link on the Bitbucket left NAVIGATION menu.
* Click Download repository
* Unzip the downloaded file
* Unzip the application folder located in: <unzip directory>\build\distributions\throttle.zip
* Copy the 4 input files: storeThrottleExample.yml, testQueriesExample.yml, testDrsExample.yml and testInitiativesExample.yml from <unzip directory>\src\main\resources to the application location: <unzip directory>\build\distributions\throttle\throttle\bin
* Launch a windows command prompt and navigate to <unzip directory>\build\distributions\throttle\throttle\bin
* Execute the following command: throttle.bat storeThrottleExample.yml testQueriesExample.yml testDrsExample.yml testInitiativesExample.yml
* The application should produce Store Load report in the same directory.
* Validate thread assignments are correct by examining storeLoadResult.yml.